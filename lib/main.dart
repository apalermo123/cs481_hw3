import 'package:flutter/material.dart';

//void main() {
//  runApp(MyApp());

//}

void main() {
  runApp(MaterialApp(
    home: MyApp(),
  ));
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Ocotillo Wells, California', style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new SecondRoute()),
                      );
                    },
                    child: Text(
                        '@Angel909',
                        style: TextStyle(
                          color: Colors.blue,
                        )
                    )
                )

              ],
            ),
          ),
          Icon(
          //FavoriteWidget(),
          Icons.account_circle,
          color: Colors.blue,
          ),
          //Text('10', style: TextStyle(color: Colors.white),),
        ],
      ),
    );

    Color color = Theme
        .of(context)
        .primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              FavoriteWidget(),
              //_buildButtonColumn(color, FavoriteWidget() , 'Like'),
              Container(
                margin: const EdgeInsets.only(top: 8),
                child: Text(
                  'Like',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
          _buildButtonColumn(color, Icons.chat, 'Comment'),
          _buildButtonColumn(color, Icons.send, 'Share'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'My brother and I took a trip out to Ocotillo Wells this weekend. We did a lot of fun things and met a lot of nice people. On Friday we even '
            'saw a cute rabbit running across the sand wash. My favorite thing to do is ride dirt bikes with my brother! Here is a photo of us from our awesome weekend. #dirtscooters ',

        style: TextStyle(color: Colors.white),
        softWrap: true,
      ),
    );

    //Widget nextPhotoButton = Container(
    //child: Row(
    //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //children: [
    //_buildButtonColumn(color, Icons.navigate_next, 'Next Photo'),
    //]
    //),
    //);

    return MaterialApp(
        title: 'Layout Homework',
        home: Scaffold(
          backgroundColor: Colors.white10,
          appBar: AppBar(
            backgroundColor: Colors.white10,
            title: Text('Layout Homework'),
          ),
          body: ListView(
              children: [
                Image.asset(
                  'DirtbikeBrothers.JPG',
                  width: 600,
                  height: 240,
                  fit: BoxFit.cover,
                ),
                titleSection,
                buttonSection,
                textSection,
                //nextPhotoButton
              ]
          ),
        )
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Angel909s Page',
        home: Scaffold(
          backgroundColor: Colors.white10,
          appBar: AppBar(
            backgroundColor: Colors.white10,
            title: Text('Angel909s Page'),
          ),
          body: ListView(
              children: [
                Image.asset(
                  'angel.JPG',
                  width: 600,
                  height: 400,
                  fit: BoxFit.cover,
                ),
                Text('My name is Angel and this is my page!', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.white,),),
          FlatButton(onPressed: () {Navigator.pop(context);},
              child: Text('Return to Home', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.blue,),),
          ),
              ]
          ),
        )
    );
  }
}

class FavoriteWidget extends StatefulWidget{
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = true;
  int _favoriteCount = 10;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFavorited ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text(
                '$_favoriteCount',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      } else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }
}